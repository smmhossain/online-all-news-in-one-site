<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>News Headline</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<style>
.bg-dark {
    background-color: #a20808!important;
}
h1 {
	color: #47319a !important;
}
h3 {
	font-weight: 700 ;
	color: #1f4363 !important;
	font-size: 25px;
	line-height: 33px;
	text-decoration: none;
}

.fakeimg {
	height: 200px;
	background: none;
	width: 100%;
}
.td-post-date {
    color: #aaa;
    display: inline-block;
    position: relative;
    top: 2px;
}
</style>
</head>
<body>

	<div class="jumbotron text-center" style="margin-bottom: 0">
		<h1>News Headlines</h1>
<!-- 	    <p>Resize this responsive page to see the effect!</p> -->
	</div>

	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<a class="navbar-brand" href="index.jsp">Home</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="collapsibleNavbar">
			<ul class="navbar-nav">
				<li class="nav-item"><a class="nav-link" href="sports.jsp">Sports News</a></li>
				<li class="nav-item"><a class="nav-link" href="indiannews.jsp">Indian News</a></li>
				<li class="nav-item"><a class="nav-link" href="uknews.jsp">UK News</a></li>
			</ul>
		</div>
	</nav>

	<div class="container" style="margin-top: 30px">
		<div class="row">
			<div class="col-sm-3">
				<h2>About Us</h2>
				<div class="fakeimg">
					<img src="images/news.jpg" height="100%" width="75%">
				</div>
				<p>This website gathers all news around the world and shows the headlines from various news sites.</p>
				
				<ul class="nav nav-pills flex-column" id="url-id">

				</ul>
				<hr class="d-sm-none">
			</div>
			<div class="col-sm-8" id='news'></div>
		</div>
	</div>

	<div class="jumbotron text-center" style="margin-bottom: 0">
		<p>© 2019 Copyright: News Headline</p>
	</div>
	<script>
		jQuery(document)
				.ready(
						function($) {
							var url = 'https://newsapi.org/v2/everything?'
									+ 'q=business&'
									+ 'apiKey=7656601f3e794edb88b4fae5ec7ba553';
							var newsdata = "";
							$
									.ajax({
										url : url,
										dataType : 'json',
										success : function(data) {

											//for ( var k in data) {
												data['articles']
														.forEach(function(
																element, index) {
															if (element['urlToImage'] != null) {
																newsdata += '<h3>'
																		+ element['title']
																		+ '</h3>';
																newsdata += '<h5>'
																		+ element['description']
																		+ '</h5>';
																newsdata +=	'<span class="td-post-date">'
																		+ element['publishedAt']
																		+ '</span>';
																newsdata += '<div class="fakeimg"><img src='
																		+ element['urlToImage']
																		+ ' width="75%" height="100%" alt="Not Loaded"></div>';
																newsdata += '<p>'
																		+ element['content']
																		+ '</p>';
																newsdata += '<a href='+element['url']+'>See More...</a><br><hr>';
															}
														});
											//}
											$('#news').html(newsdata);
										}
									});

						});
	</script>
</body>
</html>