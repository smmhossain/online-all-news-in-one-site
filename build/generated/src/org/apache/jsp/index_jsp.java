package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html lang=\"en\">\r\n");
      out.write("    <head>\r\n");
      out.write("        <title>News Headline</title>\r\n");
      out.write("        <meta charset=\"utf-8\">\r\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n");
      out.write("        <link rel=\"stylesheet\"\r\n");
      out.write("              href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\">\r\n");
      out.write("        <script\r\n");
      out.write("        src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>\r\n");
      out.write("        <script\r\n");
      out.write("        src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js\"></script>\r\n");
      out.write("        <script\r\n");
      out.write("        src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js\"></script>\r\n");
      out.write("        <style>\r\n");
      out.write("            .bg-dark {\r\n");
      out.write("                background-color: #a20808!important;\r\n");
      out.write("            }\r\n");
      out.write("            h1 {\r\n");
      out.write("                color: #47319a !important;\r\n");
      out.write("            }\r\n");
      out.write("            h3 {\r\n");
      out.write("                font-weight: 700 ;\r\n");
      out.write("                color: #1f4363 !important;\r\n");
      out.write("                font-size: 25px;\r\n");
      out.write("                line-height: 33px;\r\n");
      out.write("                text-decoration: none;\r\n");
      out.write("            }\r\n");
      out.write("\r\n");
      out.write("            .fakeimg {\r\n");
      out.write("                height: 200px;\r\n");
      out.write("                background: none;\r\n");
      out.write("                width: 100%;\r\n");
      out.write("            }\r\n");
      out.write("            .td-post-date {\r\n");
      out.write("                color: #aaa;\r\n");
      out.write("                display: inline-block;\r\n");
      out.write("                position: relative;\r\n");
      out.write("                top: 2px;\r\n");
      out.write("            }\r\n");
      out.write("        </style>\r\n");
      out.write("    </head>\r\n");
      out.write("    <body>\r\n");
      out.write("\r\n");
      out.write("        <div class=\"jumbotron text-center\" style=\"margin-bottom: 0\">\r\n");
      out.write("            <h1>News Headlines</h1>\r\n");
      out.write("            <!-- \t    <p>Resize this responsive page to see the effect!</p> -->\r\n");
      out.write("        </div>\r\n");
      out.write("\r\n");
      out.write("        <nav class=\"navbar navbar-expand-sm bg-dark navbar-dark\">\r\n");
      out.write("            <a class=\"navbar-brand\" href=\"index.jsp\">Home</a>\r\n");
      out.write("            <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\"\r\n");
      out.write("                    data-target=\"#collapsibleNavbar\">\r\n");
      out.write("                <span class=\"navbar-toggler-icon\"></span>\r\n");
      out.write("            </button>\r\n");
      out.write("            <div class=\"collapse navbar-collapse\" id=\"collapsibleNavbar\">\r\n");
      out.write("                <ul class=\"navbar-nav\">\r\n");
      out.write("                    <li class=\"nav-item\"><a class=\"nav-link\" href=\"sports.jsp\">Sports News</a></li>\r\n");
      out.write("                    <li class=\"nav-item\"><a class=\"nav-link\" href=\"indiannews.jsp\">Indian News</a></li>\r\n");
      out.write("                    <li class=\"nav-item\"><a class=\"nav-link\" href=\"uknews.jsp\">UK News</a></li>\r\n");
      out.write("                </ul>\r\n");
      out.write("            </div>\r\n");
      out.write("        </nav>\r\n");
      out.write("\r\n");
      out.write("        <div class=\"container\" style=\"margin-top: 30px\">\r\n");
      out.write("            <div class=\"row\">\r\n");
      out.write("                <div class=\"col-sm-3\">\r\n");
      out.write("                    <h2>About Us</h2>\r\n");
      out.write("                    <div class=\"fakeimg\">\r\n");
      out.write("                        <img src=\"images/news.jpg\" height=\"100%\" width=\"75%\">\r\n");
      out.write("                    </div>\r\n");
      out.write("                    <p>This website gathers all news around the world and shows the headlines from various news sites.</p>\r\n");
      out.write("\r\n");
      out.write("                    <ul class=\"nav nav-pills flex-column\" id=\"url-id\">\r\n");
      out.write("\r\n");
      out.write("                    </ul>\r\n");
      out.write("                    <hr class=\"d-sm-none\">\r\n");
      out.write("                </div>\r\n");
      out.write("                <div class=\"col-sm-8\" id='news'></div>\r\n");
      out.write("            </div>\r\n");
      out.write("        </div>\r\n");
      out.write("        <div class=\"jumbotron text-center\" style=\"margin-bottom: 0\">\r\n");
      out.write("            <p>© 2019 Copyright: News Headline</p>\r\n");
      out.write("        </div>\r\n");
      out.write("        <script>\r\n");
      out.write("            jQuery(document)\r\n");
      out.write("                    .ready(\r\n");
      out.write("                            function ($) {\r\n");
      out.write("                                var url = 'https://newsapi.org/v2/top-headlines?'\r\n");
      out.write("                                        + 'sources=bbc-news&'\r\n");
      out.write("                                        + 'apiKey=7656601f3e794edb88b4fae5ec7ba553';\r\n");
      out.write("                                var newsdata = \"\";\r\n");
      out.write("                                $\r\n");
      out.write("                                        .ajax({\r\n");
      out.write("                                            url: url,\r\n");
      out.write("                                            dataType: 'json',\r\n");
      out.write("                                            success: function (data) {\r\n");
      out.write("\r\n");
      out.write("                                                //\tfor ( var k in data) {\r\n");
      out.write("                                                data['articles']\r\n");
      out.write("                                                        .forEach(function (\r\n");
      out.write("                                                                element, index) {\r\n");
      out.write("                                                            if (element['urlToImage'] != null) {\r\n");
      out.write("                                                                newsdata += '<h3>'\r\n");
      out.write("                                                                        + element['title']\r\n");
      out.write("                                                                        + '</h3>';\r\n");
      out.write("                                                                newsdata += '<h5>'\r\n");
      out.write("                                                                        + element['description']\r\n");
      out.write("                                                                        + '</h5>';\r\n");
      out.write("                                                                newsdata += '<span class=\"td-post-date\">'\r\n");
      out.write("                                                                        + element['publishedAt']\r\n");
      out.write("                                                                        + '</span>';\r\n");
      out.write("                                                                newsdata += '<div class=\"fakeimg\"><img src='\r\n");
      out.write("                                                                        + element['urlToImage']\r\n");
      out.write("                                                                        + ' width=\"75%\" height=\"100%\" alt=\"Not Loaded\"></div>';\r\n");
      out.write("                                                                newsdata += '<p>'\r\n");
      out.write("                                                                        + element['content']\r\n");
      out.write("                                                                        + '</p>';\r\n");
      out.write("                                                                newsdata += '<a href=' + element['url'] + '>See More...</a><br><hr>';\r\n");
      out.write("                                                            }\r\n");
      out.write("                                                        });\r\n");
      out.write("                                                //}\r\n");
      out.write("                                                $('#news').html(newsdata);\r\n");
      out.write("                                            }\r\n");
      out.write("                                        });\r\n");
      out.write("\r\n");
      out.write("                            });\r\n");
      out.write("        </script>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
