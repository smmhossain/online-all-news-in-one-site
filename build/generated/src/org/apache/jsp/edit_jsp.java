package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class edit_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html lang=\"en\">\n");
      out.write("    <head>\n");
      out.write("        \n");
      out.write("        <meta charset=\"utf-8\">\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("        <link rel=\"stylesheet\"\n");
      out.write("              href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\">\n");
      out.write("        <script\n");
      out.write("        src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>\n");
      out.write("        <script\n");
      out.write("        src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js\"></script>\n");
      out.write("        <script\n");
      out.write("        src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js\"></script>\n");
      out.write("        <style>\n");
      out.write("            .bg-dark {\n");
      out.write("                background-color: #a20808!important;\n");
      out.write("            }\n");
      out.write("            h1 {\n");
      out.write("                color: #47319a !important;\n");
      out.write("            }\n");
      out.write("            h3 {\n");
      out.write("                font-weight: 700 ;\n");
      out.write("                color: #1f4363 !important;\n");
      out.write("                font-size: 25px;\n");
      out.write("                line-height: 33px;\n");
      out.write("                text-decoration: none;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            .fakeimg {\n");
      out.write("                height: 200px;\n");
      out.write("                background: none;\n");
      out.write("                width: 100%;\n");
      out.write("            }\n");
      out.write("            .td-post-date {\n");
      out.write("                color: #aaa;\n");
      out.write("                display: inline-block;\n");
      out.write("                position: relative;\n");
      out.write("                top: 2px;\n");
      out.write("            }\n");
      out.write("        </style>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("\n");
      out.write("        \n");
      out.write("\n");
      out.write("        <nav class=\"navbar navbar-expand-sm bg-dark navbar-dark\">\n");
      out.write("            <a class=\"navbar-brand\" href=\"index.jsp\">Home</a>\n");
      out.write("            <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\"\n");
      out.write("                    data-target=\"#collapsibleNavbar\">\n");
      out.write("                <span class=\"navbar-toggler-icon\"></span>\n");
      out.write("            </button>\n");
      out.write("            \n");
      out.write("        </nav>\n");
      out.write("\n");
      out.write("        <div class=\"container\" style=\"margin-top: 30px\">\n");
      out.write("            <div class=\"row\">\n");
      out.write("                <div class=\"col-sm-3\">\n");
      out.write("                  \n");
      out.write("                    <ul class=\"nav nav-pills flex-column\" id=\"url-id\">\n");
      out.write("\n");
      out.write("                    </ul>\n");
      out.write("                    <hr class=\"d-sm-none\">\n");
      out.write("                </div>\n");
      out.write("                <div class=\"col-sm-8\" id='news'></div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        <div class=\"jumbotron text-center\" style=\"margin-bottom: 0\">\n");
      out.write("            <p>© 2019 Copyright: News Headline</p>\n");
      out.write("        </div>\n");
      out.write("        <script>\n");
      out.write("            jQuery(document)\n");
      out.write("                    .ready(\n");
      out.write("                            function ($) {\n");
      out.write("                                var url = 'https://newsapi.org/v2/top-headlines?'\n");
      out.write("                                        + 'sources=bbc-news&'\n");
      out.write("                                        + 'apiKey=7656601f3e794edb88b4fae5ec7ba553';\n");
      out.write("                                var newsdata = \"\";\n");
      out.write("                                $\n");
      out.write("                                        .ajax({\n");
      out.write("                                            url: url,\n");
      out.write("                                            dataType: 'json',\n");
      out.write("                                            success: function (data) {\n");
      out.write("\n");
      out.write("                                                //\tfor ( var k in data) {\n");
      out.write("                                                data['articles']\n");
      out.write("                                                        .forEach(function (\n");
      out.write("                                                                element, index) {\n");
      out.write("                                                            if (element['urlToImage'] != null) {\n");
      out.write("                                                                newsdata += '<h3>'\n");
      out.write("                                                                        + element['title']\n");
      out.write("                                                                        + '</h3>';\n");
      out.write("                                                                newsdata += '<h5>'\n");
      out.write("                                                                        + element['description']\n");
      out.write("                                                                        + '</h5>';\n");
      out.write("                                                                newsdata += '<span class=\"td-post-date\">'\n");
      out.write("                                                                        + element['publishedAt']\n");
      out.write("                                                                        + '</span>';\n");
      out.write("                                                                newsdata += '<div class=\"fakeimg\"><img src='\n");
      out.write("                                                                        + element['urlToImage']\n");
      out.write("                                                                        + ' width=\"75%\" height=\"100%\" alt=\"Not Loaded\"></div>';\n");
      out.write("                                                                newsdata += '<p>'\n");
      out.write("                                                                        + element['content']\n");
      out.write("                                                                        + '</p>';\n");
      out.write("                                                                newsdata += '<a href=' + element['url'] + '>See More...</a><br><hr>';\n");
      out.write("                                                            }\n");
      out.write("                                                        });\n");
      out.write("                                                //}\n");
      out.write("                                                $('#news').html(newsdata);\n");
      out.write("                                            }\n");
      out.write("                                        });\n");
      out.write("\n");
      out.write("                            });\n");
      out.write("        </script>\n");
      out.write("    </body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
